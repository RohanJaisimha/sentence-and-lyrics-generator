import json
import re

# creates JSON file ("words_data.json")
# each word is a key
# and the value is a list of all words that follow it
def main():
    f_name = input("Enter your file name (with extension): ")
    fin = open(f_name, 'r') 

    dict1 = {}
    num_words_in_a_row = 2
    words = fin.read().strip().split()
    print("There are", len(words), "words.")
    print("There are", len(set(words)), "unique words.")
    count = 0
    for word in set(words):
        indices = [i for i, x in enumerate(words) if x == word]
        l = []
        count += 1
        if(count % 1000 == 0):
            print("Finished", count, "words.")
        for idx in indices:
            try:
                l.append(" ".join(words[idx+1:idx+1+num_words_in_a_row]))
            except:
                pass
        dict1[word] = l

    fout = open("words_data.json", 'w')
    json.dump(dict1, fout, sort_keys=True, indent=4)
    fout.close()
    fin.close()


if(__name__ == "__main__"):
    main()
