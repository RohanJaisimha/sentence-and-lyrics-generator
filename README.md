# About
* Generates song lyrics / sentences based on a dataset of songs, or a book
* Credit to [johnwmillr](https://github.com/johnwmillr) for the [Genius Lyrics API](https://github.com/johnwmillr/LyricsGenius)

# How to run
* Download/clone the repo onto your local device. Navigate to the directory.
* Run `pip install -r requirements.txt`
* If generating lyrics, run geniusLyricsGetter.py using `python3 geniusLyricsGetter.py`. This will create a new file, ending with "lyrics.txt"
* Depending on the number of songs that this artist has, the above step could take up to thirty minutes.
* Then, clean the text if you choose to using textCleaning.py. You can do this by typing `python3 textCleaning.py`
* Then, create words_data.json by running textJSONifier.py. You can do this by typing `python3 textJSONifier.py`
* Finally, you may run sentenceGenerator.py, by typing `python3 sentenceGenerator.py`