import lyricsgenius as genius
import json
import os

# takes in an artist's name from console
# stores lyrics in a text file, "Lyrics.txt"
def main():
    # for some reason, genius stores each song
    # in a separate file, and won't overwrite a file unless
    # enter 'y' for each file
    # so I just remove all of them first
    for i in os.listdir():
        if("lyrics_" == i[:7] and ".json" == i[-5:]):
            os.remove(i)

    artist_name = input("Enter an artist: ")
    Client_Access_Token = "RN78pDFApItzD_8QIwEVojU7HpLTszS8CcF9lBdnSVgAz6QZddJ2ft5CTV5q8yk4"

    api = genius.Genius(Client_Access_Token)
    artist = api.search_artist(artist_name)
    artist.save_lyrics()  # saves as individual jsons

    # reads each json and stores it all in a text file, "<artist_name>_lyrics.txt"
    fout = open(artist_name.lower().replace(" ", "_")+"_lyrics.txt", 'w')
    num_songs = 0
    length_of_songs = 0
    for i in os.listdir():
        if("lyrics_" != i[:7]):
            continue
        fin = open(i, 'r')
        lyrics = json.load(fin)['songs'][0]['lyrics']
        fout.write(lyrics+"\n")
        length_of_songs += len(lyrics.split())
        num_songs += 1
        fin.close()
        os.remove(i)  # deletes all the lyrics files cause their pointless
    fout.close()
    print("Length of an average song was", length_of_songs/num_songs)


if(__name__ == "__main__"):
    main()
