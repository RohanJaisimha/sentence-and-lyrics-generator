import json
import random

# get the index of the last ocurrence of value in text
def lastIndex(text, value):
    for i in range(len(text)-1, -1, -1):
        if(text[i:i+len(value)] == value):
            return i
    return -2


# formats output like how a song would be printed
# New lines if a word begins with a capital letter (except for "I")
# or if it begins and ends with square brackets in it
def printSong(text):
    count = 0
    max_words = 250
    answer_formatted = ""
    for i in text.split():
        count += 1
        if("[" == i[0] and "]" == i[-1]):
            answer_formatted = answer_formatted + "\n\n" + i + "\n"
        elif("[" == i[0]):
            answer_formatted = answer_formatted + "\n\n" + i
        elif("]" == i[-1]):
            answer_formatted = answer_formatted + i + "\n\n"
        elif(65 <= ord(i[0]) <= 90 and i != "I"):
            answer_formatted = answer_formatted + "\n" + i
        else:
            answer_formatted = answer_formatted + " " + i

    print(answer_formatted)


def main():
    fin = open("words_data.json", 'r')
    data = json.load(fin)
    fin.close()
    is_music = True if input(
        "Is this a music thing? (Y/N): ").lower() == "y" else False
    count = 0
    max_words = 250  # most songs are about 250 words or so long
    starting_word = "[Intro]" if is_music else "The"
    answer = starting_word + " "
    while(count < max_words):
        starting_word = random.choice(data[starting_word])
        answer = answer + starting_word + " "
        starting_word = starting_word.rsplit(maxsplit=1)[-1]
        count += 1
    if(is_music):
        answer = answer[:lastIndex(answer, "\n")+1]
        printSong(answer)
    else:
        answer = answer[:lastIndex(answer, ".")+1]
        print(answer)


if(__name__ == "__main__"):
    main()
