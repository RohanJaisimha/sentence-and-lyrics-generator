import re

# cleans the file by removing every non alphabetic and non period and non space
def main():
    f_name = input("Enter your file name (with extension): ")
    fin = open(f_name, 'r')
    fout = open("clean_"+f_name, 'w')

    for line in fin:
        line = line.strip()
        line = re.sub("[^\w. ]", "", line)
        fout.write(line+"\n")

    fin.close()
    fout.close()


if(__name__ == "__main__"):
    main()
